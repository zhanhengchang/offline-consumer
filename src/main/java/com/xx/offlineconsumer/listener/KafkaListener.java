package com.xx.offlineconsumer.listener;

import com.xx.offlineconsumer.util.FileUtil;
import com.xx.offlineconsumer.util.ZookeeperUtil;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class KafkaListener implements CommandLineRunner {
    private static String GROUP_ID = "offline-consumer-test";
    private static String TOPIC = "ctrip_clean";
    private static String KAFKA_BROKERS = "192.168.100.51:9092,192.168.100.52:9092,192.168.100.53:9092";
    private static ZooKeeper zooKeeper = ZookeeperUtil.getZkClient();
    private static String GLOBAL_PATH = String.format("/consumers/%s/offsets/%s", GROUP_ID, TOPIC);
    public static List<Integer> PARTITION = Arrays.asList(0, 1, 2);
    public static List<PrintWriter> printWriterList = FileUtil.getPrintWriterList();

    @Override
    public void run(String... args) {   //程序启动就执行监听任务
        if (PARTITION.size() > 0) {
            ExecutorService executorService = Executors.newFixedThreadPool(PARTITION.size());

            try {   //初始化Zookeeper路径
                Stat stat1 = zooKeeper.exists("/consumers/"+GROUP_ID, true);
                if (stat1 == null) {
                    ZookeeperUtil.createNode("/consumers/"+GROUP_ID, "0");
                }
                Stat stat2 = zooKeeper.exists("/consumers/"+GROUP_ID+"/offsets", true);
                if (stat2 == null) {
                    ZookeeperUtil.createNode("/consumers/"+GROUP_ID+"/offsets", "0");
                }
                Stat stat3 = zooKeeper.exists(GLOBAL_PATH, true);
                if (stat3 == null) {
                    ZookeeperUtil.createNode(GLOBAL_PATH, "0");
                }

                List<String> children = zooKeeper.getChildren(GLOBAL_PATH, true);
                if (children.isEmpty()) {
                    PARTITION.forEach( x -> {
                        try {
                            ZookeeperUtil.createNode(GLOBAL_PATH + "/" + (Integer)x, "0");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    System.out.println("ZK节点初始化完成！");
                }

                PARTITION.forEach(x -> executorService.submit(() -> startConsume((Integer) x, TOPIC)));
            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    private void startConsume(int partitionIndex, String topic) {
        Properties properties = buildKafkaConfig();
        //创建kafka consumer
        KafkaConsumer<String, String> consumer = new KafkaConsumer(properties);

        try {
            //指定该consumer对应的消费分区
            TopicPartition partition = new TopicPartition(topic, partitionIndex);
            consumer.assign(Arrays.asList(partition));

            //取得指定Topic、指定Partition的offset
            Stat stat = new Stat(); //用这个来解决数据的版本问题
            String offset = new String(zooKeeper.getData(GLOBAL_PATH + "/" + partition.partition(), null, stat));
            System.out.println("读到offset="+offset);
            System.out.println("读到路径为"+GLOBAL_PATH + "/" + partition.partition());
            //consumer的offset处理
            Long seekOffset = Long.valueOf(offset);
            consumer.seek(partition, seekOffset);

            //开始消费数据任务
            kafkaRecordConsume(consumer, partition.partition());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Properties buildKafkaConfig() {
        Properties props = new Properties();

        props.put("auto.offset.reset", "latest");
        //集群地址，多个地址用"，"分隔
        props.put("bootstrap.servers", KAFKA_BROKERS);
        //设置消费者的group id
        props.put("group.id", GROUP_ID);
        //如果为真，consumer所消费消息的offset将会自动的同步到zookeeper。如果消费者死掉时，由新的consumer使用继续接替
        props.put("enable.auto.commit", "false");
        //consumer向zookeeper提交offset的频率
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        //反序列化
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");

        return props;
    }

    private void kafkaRecordConsume(KafkaConsumer<String, String> consumer, int partition) {//消费数据并提交offset
        while (true) {
            try {
                ConsumerRecords<String, String> consumerRecords = consumer.poll(100);
                for(ConsumerRecord<String,String> consumerRecord : consumerRecords){
//                    System.out.println("在topic为test中读到：" + consumerRecord.value()+"\toffset="+consumerRecord.offset());
                    FileUtil.saveLogs(printWriterList.get(partition), consumerRecord.value());      //  下一次的consumerRecord.offset() + 1L
                    Stat stat = new Stat(); //用这个来解决数据的版本问题
                    zooKeeper.getData(GLOBAL_PATH + "/" + partition, null, stat);
                    zooKeeper.setData(GLOBAL_PATH + "/" + partition, ((consumerRecord.offset() + 1L)+"").getBytes(), stat.getVersion());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    //这个地方需要频繁的new Stat()对象，考虑JVM优化
}