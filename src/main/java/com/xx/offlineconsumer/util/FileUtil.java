package com.xx.offlineconsumer.util;

import com.xx.offlineconsumer.listener.KafkaListener;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class FileUtil {
    private volatile static List<PrintWriter> printWriterList = new ArrayList<>();

    private static String PARENT_PATH = "/root/ctrip/";
    private static String FLUME_SRC_PATH = "/root/flume/";
    private static SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-DD");
    private static File parent = new File(PARENT_PATH);
    private static File flume_src = new File(FLUME_SRC_PATH);

    static {
        if ( !parent.exists() ) {
            parent.mkdirs();
        }
        if ( !flume_src.exists() ) {
            flume_src.mkdirs();
        }
        createLogFile();//类一被加载就应该创建出这个文件，不能等定时任务，否则Kafka消费报printWriter空指针
    }


    //日志落地到磁盘，每天00:00切换新文件，00:05将前一天的文件移动到Flume收集的源文件夹
//    @Scheduled(cron = "0 0/1 * * * ?")//先测试每分钟的,每分钟产生一个文件，这分钟的第20秒移动该文件
    @Scheduled(cron = "0 0 0 1/1 * ?")
    public static void createLogFile(){ //希望每天00:00创建出当天的日志文件，并更改文件输入流
        List<Integer> list = KafkaListener.PARTITION;
        printWriterList.clear();
        String fileName = sdf.format(new Date());

        list.forEach(x -> {
            String path = PARENT_PATH + fileName + "-" + x + ".log";
            File file = new File(path);
            try {
                if ( !file.exists() ) {
                    file.createNewFile();
                }
                PrintWriter printWriter = new PrintWriter(new FileOutputStream(path));
                printWriterList.add(printWriter);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }


    @Scheduled(cron = "0 5 0 1/1 * ?")  //每天的00:05执行移动任务
    public void moveFile() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -1);    //得到前一天
        String yesterday = sdf.format(calendar.getTime());
        List<Integer> list = KafkaListener.PARTITION;
        list.forEach(x -> {
            String path = PARENT_PATH + yesterday + "-" + x + ".log";
            File file = new File(path);
            if (file.exists()){
                file.renameTo(new File(FLUME_SRC_PATH + yesterday + "-" + x + ".log"));//移动到Flume收集的源目录下
            }
        });
    }


//    @Scheduled(cron = "20 * * * * ?")//每分钟的第20秒
//    public void moveFile() {
//        String time = sdf.format(new Date());
//        String minute = String.format("%s00", time.substring(0, time.length()-2));//当前分钟
//        String path = PARENT_PATH + minute;
//        File file = new File(path);
//        if (file.exists()){
//            file.renameTo(new File(FLUME_SRC_PATH + minute));//移动到Flume收集的源目录下
//        }
//    }


//    @KafkaListener(topics = TOPIC)
//    public void onMessage(String message) {
//        saveLogs(printWriter, message);
//    }


    public static void saveLogs(PrintWriter printWriter, String message) {
        printWriter.print(message + "\n");//每天的日志形成一份文件
        printWriter.flush();        //这份文件最后会虽然有一个空行，不过在load进Hive的时候，
                                    // 最后这个空行无影响，测试过，不会在Hive表最后产生一行NULL
    }

    public static List<PrintWriter> getPrintWriterList(){
        return printWriterList;
    }
}