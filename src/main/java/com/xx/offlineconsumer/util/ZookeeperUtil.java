package com.xx.offlineconsumer.util;

import org.apache.zookeeper.*;
import java.io.IOException;

public class ZookeeperUtil {
    private static String ZK_HOSTS = "192.168.100.51:2181,192.168.100.52:2181,192.168.100.53:2181";
    private static final int TIME_OUT = 2000;
    private static ZooKeeper zooKeeper = null;

    static {
        Watcher watcher = new Watcher() {
            @Override
            public void process(WatchedEvent event) {
                System.out.println("监听开始执行，正在监听" + event.getPath());
            }
        };
        try {
            zooKeeper = new ZooKeeper(ZK_HOSTS, TIME_OUT, watcher);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createNode(String path, String data) throws Exception {
        String result = zooKeeper.create(path, data.getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
        System.out.println(result);
    }

    public static ZooKeeper getZkClient() {
        return zooKeeper;
    }
}